﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class Manifestacija
{
    public string Naziv { get; set; }
    public TipManifestacije TipManifestacije { get; set; }
    public int BrojMesta { get; set; }
    public DateTime DatumVremeOdrzavanja { get; set; }
    public int CenaKarte { get; set; }
    public bool Status { get; set; }
    public Lokacija MestoOdrzavanja { get; set; }
    public string PosterManifestacije { get; set; }
    public string Prodavac { get; set; }
    public int Id { get; set; }
    public int BrojSlobodnihMesta { get; set; }
    public double ProsecnaOcena { get; set; }
    public int BrojKomentara { get; set; }
    public Manifestacija()
    {
        MestoOdrzavanja = new Lokacija();
    }
}
