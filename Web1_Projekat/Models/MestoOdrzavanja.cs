﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class MestoOdrzavanja
{
    public string Ulica { get; set; }
    public int Broj { get; set; }
    public string Mesto { get; set; }
    public int PostanskiBroj { get; set; }
    public MestoOdrzavanja()
    {

    }
}
