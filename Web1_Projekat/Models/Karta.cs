﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Karta
{
    public string JedinstveniIdentifikatorKarte { get; set; }
    public Manifestacija Manifestacija { get; set; }
    public DateTime DatumOdrzavanja { get; set; }
    public float Cena { get; set; }
    public string Kupac { get; set; }
    public string KorisnickoIme { get; set; }
    public bool Status { get; set; }
    public Tip Tip { get; set; }
    public int BrojKarata { get; set; }

    public Karta()
    {
        Manifestacija = new Manifestacija();
    }
}
