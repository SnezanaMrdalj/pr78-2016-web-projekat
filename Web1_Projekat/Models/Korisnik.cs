﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc.Html;

public class Korisnik
{
    [Required(ErrorMessage = "Unesite korisnicko ime"), MinLength(1)]
    public string KorisnickoIme { get; set; }
    [Required(ErrorMessage = "Unesite lozinku")]
    public string Lozinka { get; set; }
    public string Ime { get; set; }
    public string Prezime { get; set; }
    public string Pol { get; set; }
    public DateTime DatumRodjenja { get; set; }
    public Uloga Uloga { get; set; }
    public List<Karta> SveKarte { get; set; }
    public List<Manifestacija> Manifestacija { get; set; }
    public int BrojSakupljenihBodova { get; set; }
    public ImeTipa TipKorisnika { get; set; }

    public Korisnik() {
        SveKarte = new List<Karta>();
        TipKorisnika = ImeTipa.Bronzani;
    }
    
    public Korisnik(string korisnickoIme, string lozinka, string ime, string prezime)
    {
        KorisnickoIme = korisnickoIme;
        Lozinka = lozinka;
        Ime = ime;
        Prezime = prezime;
        TipKorisnika = ImeTipa.Bronzani;
        SveKarte = new List<Karta>();
    }
    
    
}
