﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Komentar
{
    public string Kupac { get; set; }
    public int Manifestacija { get; set; }
    public string TekstKomentara { get; set; }
    public int Ocena { get; set; }
    public bool Status { get; set; }
    public int Id { get; set; }
    public Komentar()
    {
        Status = false;
    }
}
