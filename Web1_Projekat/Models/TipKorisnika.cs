﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class TipKorisnika
{
    public ImeTipa ImeTipa { get; set; }
    public int Popust { get; set; }
    public int TrazeniBrojBodova { get; set; }

    public TipKorisnika()
    {

    }
}
