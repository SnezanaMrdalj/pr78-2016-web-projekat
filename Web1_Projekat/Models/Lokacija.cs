﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Lokacija
{
    public string GeografskaDuzina { get; set; }
    public string GeografskaSirina { get; set; }
    public MestoOdrzavanja MestoOdrzavanja { get; set; }
    public int Manifestacija { get; set; }
    public Lokacija()
    {
        MestoOdrzavanja = new MestoOdrzavanja();
    }

    public override string ToString()
    {
        return MestoOdrzavanja.Ulica + " " + MestoOdrzavanja.Broj + "\n " + MestoOdrzavanja.PostanskiBroj + " " +MestoOdrzavanja.Mesto + "\n" + GeografskaDuzina + " " + GeografskaSirina;
    }
}
