﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

public class Baza
{
    public Baza()
    {

    }

    public Dictionary<string, Korisnik> VratiAdmine()
    {
        string line;
        Dictionary<string, Korisnik> lista = new Dictionary<string, Korisnik>();


        System.IO.StreamReader file = new System.IO.StreamReader(HttpContext.Current.Server.MapPath("~") + @"\Admini.txt");
        while ((line = file.ReadLine()) != null)
        {
            string[] podeljeno = line.Split('|');
            if (!lista.ContainsKey(podeljeno[0]))
            {

                lista.Add(podeljeno[0], new Korisnik(podeljeno[0], podeljeno[1], podeljeno[2], podeljeno[3]) { Uloga = Uloga.Administrator, Pol = podeljeno[4], DatumRodjenja = DateTime.Parse(podeljeno[5]) });

            }
        }
        file.Close();

        return lista;
    }

    public void ListaKorisnika(Dictionary<string, Korisnik> lista)
    {
        XmlWriter xmlWriter = XmlWriter.Create(HttpContext.Current.Server.MapPath("~") + "/ListaKorisnika.xml");

        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement("korisnici");


        foreach (var item in lista.Values)
        {
            if (item.Uloga != Uloga.Administrator)
            {
                xmlWriter.WriteStartElement("korisnik");
                xmlWriter.WriteStartElement("KorisnickoIme");
                xmlWriter.WriteString(item.KorisnickoIme);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Lozinka");
                xmlWriter.WriteString(item.Lozinka);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Ime");
                xmlWriter.WriteString(item.Ime);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Prezime");
                xmlWriter.WriteString(item.Prezime);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Pol");
                xmlWriter.WriteString(item.Pol);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("DatumRodjenja");
                xmlWriter.WriteString(item.DatumRodjenja.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Uloga");
                xmlWriter.WriteString(item.Uloga.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("BrojSakupljenihBodova");
                xmlWriter.WriteString(item.BrojSakupljenihBodova.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("TipKorisnika");
                xmlWriter.WriteString(item.TipKorisnika.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
            }

        }
        xmlWriter.WriteEndDocument();
        xmlWriter.Close();
    }


    public Dictionary<string, Korisnik> UcitajKorisnike()
    {
        Dictionary<string, Korisnik> lista = new Dictionary<string, Korisnik>();
        string podaci = HttpContext.Current.Server.MapPath("~/ListaKorisnika.xml");
        List<Karta> listaKarata = (List<Karta>)(HttpContext.Current.Cache["karte"]);

        XmlDocument xmlDocument = new XmlDocument();
        try
        {
            xmlDocument.Load(podaci);
            foreach (XmlNode node in xmlDocument.DocumentElement.ChildNodes)
            {
                Korisnik korisnik = new Korisnik();
                XmlNode node1 = node.FirstChild;
                if (!lista.ContainsKey(node1.InnerText))
                {
                    korisnik.KorisnickoIme = node1.InnerText;
                    node1 = node1.NextSibling;
                    korisnik.Lozinka = node1.InnerText;
                    node1 = node1.NextSibling;
                    korisnik.Ime = node1.InnerText;
                    node1 = node1.NextSibling;
                    korisnik.Prezime = node1.InnerText;
                    node1 = node1.NextSibling;
                    korisnik.Pol = node1.InnerText;
                    node1 = node1.NextSibling;
                    korisnik.DatumRodjenja = DateTime.Parse(node1.InnerText);
                    node1 = node1.NextSibling;

                    if (node1.InnerText == "Prodavac")
                    {
                        korisnik.Uloga = Uloga.Prodavac;
                        korisnik.Manifestacija = new List<Manifestacija>();
                    }
                    else
                    {
                        korisnik.Uloga = Uloga.Kupac;
                        korisnik.SveKarte = new List<Karta>();

                    }

                    node1 = node1.NextSibling;
                    korisnik.BrojSakupljenihBodova = Int32.Parse(node1.InnerText);

                    foreach (var karta in listaKarata)
                    {
                        if (karta.KorisnickoIme == korisnik.KorisnickoIme)
                        {
                            korisnik.SveKarte.Add(karta);
                        }
                    }

                    node1 = node1.NextSibling;
                    if (node1.InnerText == ImeTipa.Bronzani.ToString())
                    {
                        korisnik.TipKorisnika = ImeTipa.Bronzani;
                    } else if (node1.InnerText == ImeTipa.Srebrni.ToString())
                    {
                        korisnik.TipKorisnika = ImeTipa.Srebrni;
                    } else
                    {
                        korisnik.TipKorisnika = ImeTipa.Zlatni;
                    }


                        lista.Add(korisnik.KorisnickoIme, korisnik);

                }
            }

        }
        catch (Exception)
        {


        }
        return lista;

    }

    //svi korisnici (Admini i Korisnici)
    public Dictionary<string, Korisnik> UcitajSveKorisnike()
    {
        Dictionary<string, Korisnik> lista = new Dictionary<string, Korisnik>();

        Dictionary<string, Korisnik> listaAdmina = VratiAdmine();

        Dictionary<string, Korisnik> listKorisnika = UcitajKorisnike();

        foreach (var svi in listaAdmina.Values)
        {
            lista.Add(svi.KorisnickoIme, svi);
        }

        foreach (var svi in listKorisnika.Values)
        {
            lista.Add(svi.KorisnickoIme, svi);
        }

        return lista;
    }

    public List<Manifestacija> UcitajManifestacije()
    {
        List<Manifestacija> lista = new List<Manifestacija>();

        string podaci = HttpContext.Current.Server.MapPath("~/ListaManifestacija.xml");

        XmlDocument xmlDocument = new XmlDocument();
        try
        {
            xmlDocument.Load(podaci);
            foreach (XmlNode node in xmlDocument.DocumentElement.ChildNodes)
            {
                Manifestacija manifestacija = new Manifestacija();
                XmlNode node1 = node.FirstChild;



                manifestacija.Naziv = node1.InnerText;
                node1 = node1.NextSibling;
                if (node1.InnerText == TipManifestacije.Festival.ToString())
                {
                    manifestacija.TipManifestacije = TipManifestacije.Festival;
                }
                else if (node1.InnerText == TipManifestacije.Koncert.ToString())
                {
                    manifestacija.TipManifestacije = TipManifestacije.Koncert;
                }
                else if (node1.InnerText == TipManifestacije.Pozoriste.ToString())
                {
                    manifestacija.TipManifestacije = TipManifestacije.Pozoriste;
                }
                node1 = node1.NextSibling;
                manifestacija.BrojMesta = Int32.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                manifestacija.DatumVremeOdrzavanja = DateTime.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                manifestacija.CenaKarte = Int32.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                if (node1.InnerText == true.ToString())
                {
                    manifestacija.Status = true;
                }
                else
                {
                    manifestacija.Status = false;
                }

                node1 = node1.NextSibling;
                manifestacija.PosterManifestacije = node1.InnerText;

                node1 = node1.NextSibling;
                manifestacija.Prodavac = node1.InnerText;

                node1 = node1.NextSibling;
                manifestacija.Id = Int32.Parse(node1.InnerText);

                List<Lokacija> lokacijas = (List<Lokacija>)(HttpContext.Current.Cache["lokacije"]);
                
                foreach(var item in lokacijas)
                {
                    if(manifestacija.Id == item.Manifestacija)
                    {
                        manifestacija.MestoOdrzavanja = item;
                    }
                }
                node1 = node1.NextSibling;
                manifestacija.BrojSlobodnihMesta = Int32.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                manifestacija.ProsecnaOcena = Int32.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                manifestacija.BrojKomentara = Int32.Parse(node1.InnerText);
                lista.Add(manifestacija);


            }

        }
        catch (Exception)
        {


        }


        return lista;
    }

    public void ListaManifestacija(List<Manifestacija> lista)
    {
        XmlWriter xmlWriter = XmlWriter.Create(HttpContext.Current.Server.MapPath("~") + "/ListaManifestacija.xml");

        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement("manifestacije");


        foreach (var item in lista)
        {
            xmlWriter.WriteStartElement("manifestacija");
            xmlWriter.WriteStartElement("Naziv");
            xmlWriter.WriteString(item.Naziv);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("TipManifestacije");
            xmlWriter.WriteString(item.TipManifestacije.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("BrojMesta");
            xmlWriter.WriteString(item.BrojMesta.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("DatumVremeOdrzavanja");
            xmlWriter.WriteString(item.DatumVremeOdrzavanja.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("CenaKarte");
            xmlWriter.WriteString(item.CenaKarte.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Status");
            xmlWriter.WriteString(item.Status.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("PosterManifestacije");
            xmlWriter.WriteString(item.PosterManifestacije);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Prodavac");
            xmlWriter.WriteString(item.Prodavac);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Id");
            xmlWriter.WriteString(item.Id.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("BrojSlododnihMesta");
            xmlWriter.WriteString(item.BrojSlobodnihMesta.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("ProsecnaOcena");
            xmlWriter.WriteString(item.ProsecnaOcena.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("BrojKomentara");
            xmlWriter.WriteString(item.BrojKomentara.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();


        }
        xmlWriter.WriteEndDocument();
        xmlWriter.Close();
    }

    public List<Karta> UcitajKarte()
    {
        List<Karta> lista = new List<Karta>();

        string podaci = HttpContext.Current.Server.MapPath("~/ListaKarata.xml");
        List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Current.Cache["manifestacije"]);
        XmlDocument xmlDocument = new XmlDocument();
        try
        {
            xmlDocument.Load(podaci);

            foreach (XmlNode node in xmlDocument.DocumentElement.ChildNodes)
            {
                Karta karta = new Karta();
                XmlNode node1 = node.FirstChild;

                karta.JedinstveniIdentifikatorKarte = node1.InnerText;
                node1 = node1.NextSibling;
                karta.DatumOdrzavanja = DateTime.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                karta.Cena = Int32.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                karta.Kupac = node1.InnerText;
                node1 = node1.NextSibling;

                if (node1.InnerText == true.ToString())
                {
                    karta.Status = true;
                }
                else
                {
                    karta.Status = false;
                }
                node1 = node1.NextSibling;

                if (node1.InnerText == Tip.FAN_PIT.ToString())
                {
                    karta.Tip = Tip.FAN_PIT;
                }
                else if (node1.InnerText == Tip.REGULAR.ToString())
                {
                    karta.Tip = Tip.REGULAR;
                }
                else if (node1.InnerText == Tip.VIP.ToString())
                {
                    karta.Tip = Tip.VIP;
                }

                node1 = node1.NextSibling;
                karta.BrojKarata = Int32.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                foreach(var manifestacija in manifestacijas)
                {
                    if(manifestacija.Id == Int32.Parse(node1.InnerText))
                    {
                        karta.Manifestacija = manifestacija;
                    }
                }


                node1 = node1.NextSibling;
                karta.KorisnickoIme = node1.InnerText;
                
                lista.Add(karta);


            }

        }
        catch (Exception)
        {


        }


        return lista;
    }

    public void ListaKarata(List<Karta> lista)
    {
        XmlWriter xmlWriter = XmlWriter.Create(HttpContext.Current.Server.MapPath("~") + "/ListaKarata.xml");

        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement("karte");


        foreach (var item in lista)
        {
            xmlWriter.WriteStartElement("karta");
            xmlWriter.WriteStartElement("JedinstveniIdentifikatorKarte");
            xmlWriter.WriteString(item.JedinstveniIdentifikatorKarte);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("DatumOdrzavanja");
            xmlWriter.WriteString(item.DatumOdrzavanja.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Cena");
            xmlWriter.WriteString(item.Cena.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Kupac");
            xmlWriter.WriteString(item.Kupac);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Status");
            xmlWriter.WriteString(item.Status.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Tip");
            xmlWriter.WriteString(item.Tip.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("BrojKarata");
            xmlWriter.WriteString(item.BrojKarata.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Manifestacija");
            xmlWriter.WriteString(item.Manifestacija.Id.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("KorisnickoIme");
            xmlWriter.WriteString(item.KorisnickoIme.ToString());
            xmlWriter.WriteEndElement();
           
            xmlWriter.WriteEndElement();


        }
        xmlWriter.WriteEndDocument();
        xmlWriter.Close();
    }

    public List<Komentar> UcitajKomentare()
    {
        List<Komentar> lista = new List<Komentar>();

        string podaci = HttpContext.Current.Server.MapPath("~/ListaKomentara.xml");

        XmlDocument xmlDocument = new XmlDocument();
        try
        {
            xmlDocument.Load(podaci);
            foreach (XmlNode node in xmlDocument.DocumentElement.ChildNodes)
            {
                Komentar komentar = new Komentar();
                XmlNode node1 = node.FirstChild;
                komentar.Kupac = node1.InnerText;

                node1 = node1.NextSibling;
                komentar.Manifestacija = Int32.Parse(node1.InnerText);

                node1 = node1.NextSibling;
                komentar.TekstKomentara = node1.InnerText;

                node1 = node1.NextSibling;
                komentar.Ocena = Int32.Parse(node1.InnerText);
                node1 = node1.NextSibling;
                if (node1.InnerText == true.ToString())
                {
                    komentar.Status = true;
                }
                else
                {
                    komentar.Status = false;
                }
                node1 = node1.NextSibling;
                komentar.Id = Int32.Parse(node1.InnerText);

                lista.Add(komentar);


            }

        }
        catch (Exception)
        {


        }


        return lista;
    }

    public void ListaKomentara(List<Komentar> lista)
    {
        XmlWriter xmlWriter = XmlWriter.Create(HttpContext.Current.Server.MapPath("~") + "/ListaKomentara.xml");

        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement("komentari");


        foreach (var item in lista)
        {
            xmlWriter.WriteStartElement("komentar");
            xmlWriter.WriteStartElement("Kupac");
            xmlWriter.WriteString(item.Kupac);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Manifestacija");
            xmlWriter.WriteString(item.Manifestacija.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("TekstKomentara");
            xmlWriter.WriteString(item.TekstKomentara);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Ocena");
            xmlWriter.WriteString(item.Ocena.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Status");
            xmlWriter.WriteString(item.Status.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Id");
            xmlWriter.WriteString(item.Id.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();


        }
        xmlWriter.WriteEndDocument();
        xmlWriter.Close();
    }

    public List<Lokacija> UcitajLokaciju()
    {
        List<Lokacija> lista = new List<Lokacija>();

        string podaci = HttpContext.Current.Server.MapPath("~/ListaLokacija.xml");

        XmlDocument xmlDocument = new XmlDocument();
        try
        {
            xmlDocument.Load(podaci);
            foreach (XmlNode node in xmlDocument.DocumentElement.ChildNodes)
            {
                Lokacija lokacija = new Lokacija();
                XmlNode node1 = node.FirstChild;
                lokacija.GeografskaDuzina = node1.InnerText;

                node1 = node1.NextSibling;
                lokacija.GeografskaSirina = node1.InnerText;

                node1 = node1.NextSibling;
                MestoOdrzavanja mestoOdrzavanja = new MestoOdrzavanja();

                XmlNode node2 = node1.FirstChild;

                mestoOdrzavanja.Ulica = node2.InnerText;
                node2 = node2.NextSibling;
                mestoOdrzavanja.Broj = Int32.Parse(node2.InnerText);
                node2 = node2.NextSibling;
                mestoOdrzavanja.Mesto = node2.InnerText;
                node2 = node2.NextSibling;
                mestoOdrzavanja.PostanskiBroj = Int32.Parse(node2.InnerText);

                lokacija.MestoOdrzavanja = mestoOdrzavanja;

                node1 = node1.NextSibling;
                lokacija.Manifestacija = Int32.Parse(node1.InnerText);

                lista.Add(lokacija);


            }

        }
        catch (Exception)
        {


        }


        return lista;
    }

    public void ListaLokacija(List<Lokacija> lista)
    {
        XmlWriter xmlWriter = XmlWriter.Create(HttpContext.Current.Server.MapPath("~") + "/ListaLokacija.xml");

        xmlWriter.WriteStartDocument();
        xmlWriter.WriteStartElement("lokacije");


        foreach (var item in lista)
        {
            xmlWriter.WriteStartElement("lokacija");
            xmlWriter.WriteStartElement("GeografskaDuzina");
            xmlWriter.WriteString(item.GeografskaDuzina);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("GeografskaSirina");
            xmlWriter.WriteString(item.GeografskaSirina);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("MestoOdrzavanja");
            xmlWriter.WriteStartElement("Ulica");
            xmlWriter.WriteString(item.MestoOdrzavanja.Ulica);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Broj");
            xmlWriter.WriteString(item.MestoOdrzavanja.Broj.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Mesto");
            xmlWriter.WriteString(item.MestoOdrzavanja.Mesto);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("PostanskiBroj");
            xmlWriter.WriteString(item.MestoOdrzavanja.PostanskiBroj.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("Manifestacija");
            xmlWriter.WriteString(item.Manifestacija.ToString());
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();


        }
        xmlWriter.WriteEndDocument();
        xmlWriter.Close();
    }

}

