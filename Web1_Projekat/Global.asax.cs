﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Web1_Projekat
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Baza baza = new Baza();
            //poziva se samo jednom

            HttpContext.Current.Cache["lokacije"] = baza.UcitajLokaciju();
            HttpContext.Current.Cache["komentari"] = baza.UcitajKomentare();
            HttpContext.Current.Cache["manifestacije"] = baza.UcitajManifestacije();
            HttpContext.Current.Cache["karte"] = baza.UcitajKarte();
            HttpContext.Current.Cache["korisnici"] = baza.UcitajSveKorisnike();
        }
    }
}
