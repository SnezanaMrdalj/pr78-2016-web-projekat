﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web1_Projekat.Controllers
{
    public class KupacController : Controller
    {
        Baza baza = new Baza();
        // GET: Kupac
        public ActionResult Index()
        {
            return RedirectToAction("PocetnaStrana", "LogIn");
        }

        public Random random = new Random();
        public string RandomString()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        [HttpPost]
        public ActionResult Sacuvaj()
        {

            List<Karta> listaKarata = (List<Karta>)(HttpContext.Cache["karte"]);
            Karta karta = (Karta)(Session["trenutnaKarta"]);
            List<Manifestacija> listaManifestacija = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            Dictionary<string, Korisnik> listaKorisnika = (Dictionary<string, Korisnik>)(HttpContext.Cache["korisnici"]);

            do
            {
                string broj = RandomString();
                bool postoji = false;

                foreach (var karta1 in listaKarata)
                {
                    if (karta1.JedinstveniIdentifikatorKarte == broj)
                    {
                        postoji = true;

                    }
                }

                if (!postoji)
                {
                    karta.JedinstveniIdentifikatorKarte = broj;
                }

            } while (karta.JedinstveniIdentifikatorKarte == null || karta.JedinstveniIdentifikatorKarte == "");

            foreach (var manifestacija in listaManifestacija)
            {
                if (manifestacija.Id == karta.Manifestacija.Id)
                {
                    manifestacija.BrojSlobodnihMesta -= karta.BrojKarata;
                }
            }

            listaKorisnika[((Korisnik)Session["ulogovan"]).KorisnickoIme].SveKarte.Add(karta);
            listaKorisnika[((Korisnik)Session["ulogovan"]).KorisnickoIme].BrojSakupljenihBodova = (Int32)(karta.Cena / 1000 * 133);

            if (listaKorisnika[((Korisnik)Session["ulogovan"]).KorisnickoIme].BrojSakupljenihBodova > 3000)
            {
                listaKorisnika[((Korisnik)Session["ulogovan"]).KorisnickoIme].TipKorisnika = ImeTipa.Srebrni;
            }
            else if (listaKorisnika[((Korisnik)Session["ulogovan"]).KorisnickoIme].BrojSakupljenihBodova > 4000)
            {
                listaKorisnika[((Korisnik)Session["ulogovan"]).KorisnickoIme].TipKorisnika = ImeTipa.Zlatni;
            }



            Session["ulogovan"] = listaKorisnika[((Korisnik)Session["ulogovan"]).KorisnickoIme];
            HttpContext.Cache["korisnici"] = listaKorisnika;
            baza.ListaKorisnika(listaKorisnika);
            HttpContext.Cache["manifestacije"] = listaManifestacija;
            baza.ListaManifestacija(listaManifestacija);
            listaKarata.Add(karta);
            HttpContext.Cache["karte"] = listaKarata;
            baza.ListaKarata(listaKarata);

            return RedirectToAction("Index");
        }


        [HttpPost]
        public ActionResult Rezervisi()
        {

            int BrojKarata = Int32.Parse(Request["BrojKarata"]);
            int Id = Int32.Parse(Request["Id"]);
            string tip = Request["Tip"];



            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            Karta karta = new Karta();
            Manifestacija manifestacija1 = new Manifestacija();
            Korisnik korisnik = (Korisnik)(Session["ulogovan"]);

            karta.BrojKarata = BrojKarata;

            foreach (var manifestacija in manifestacijas)
            {
                if (manifestacija.Id == Id)
                {
                    manifestacija1 = manifestacija;
                }
            }

            if (manifestacija1.BrojSlobodnihMesta - BrojKarata < 0)
            {
                return RedirectToAction("Index");
            }

            karta.Manifestacija = manifestacija1;
            if (tip == Tip.REGULAR.ToString())
            {
                if (korisnik.TipKorisnika == ImeTipa.Zlatni)
                {
                    karta.Cena = (float)((manifestacija1.CenaKarte * BrojKarata) * 0.95);

                }
                else if (korisnik.TipKorisnika == ImeTipa.Srebrni)
                {
                    karta.Cena = (float)((manifestacija1.CenaKarte * BrojKarata) * 0.97);

                }
                else
                {
                    karta.Cena = manifestacija1.CenaKarte * BrojKarata;

                }
                karta.Tip = Tip.REGULAR;
            }
            else if (tip == Tip.VIP.ToString())
            {
                if (korisnik.TipKorisnika == ImeTipa.Zlatni)
                {
                    karta.Cena = (float)((manifestacija1.CenaKarte * 4 * BrojKarata) * 0.95);

                }
                else if (korisnik.TipKorisnika == ImeTipa.Srebrni)
                {
                    karta.Cena = (float)((manifestacija1.CenaKarte * 4 * BrojKarata) * 0.97);

                }
                else
                {
                    karta.Cena = manifestacija1.CenaKarte * 4 * BrojKarata;

                }

                karta.Tip = Tip.VIP;
            }
            else
            {
                if (korisnik.TipKorisnika == ImeTipa.Zlatni)
                {
                    karta.Cena = (float)((manifestacija1.CenaKarte * 2 * BrojKarata) * 0.95);

                }
                else if (korisnik.TipKorisnika == ImeTipa.Srebrni)
                {
                    karta.Cena = (float)((manifestacija1.CenaKarte * 2 * BrojKarata) * 0.97);

                }
                else
                {
                    karta.Cena = manifestacija1.CenaKarte * BrojKarata * 2;

                }

                karta.Tip = Tip.FAN_PIT;
            }

            karta.Kupac = korisnik.Ime + " " + korisnik.Prezime;
            karta.KorisnickoIme = korisnik.KorisnickoIme;
            karta.DatumOdrzavanja = manifestacija1.DatumVremeOdrzavanja;
            karta.Manifestacija.Naziv = manifestacija1.Naziv;
            ViewBag.Karta = karta;
            Session["trenutnaKarta"] = karta;

            return View("DetaljiKarte");
        }


        [HttpGet]
        public ActionResult Karte()
        {
            List<Karta> listaKarata = ((Korisnik)(Session["ulogovan"])).SveKarte;
            List<Karta> kartas = new List<Karta>();
            
            foreach (var karta in listaKarata)
            {
                if (karta.Status == false)
                {
                    kartas.Add(karta);
                }
            }
            
            ViewBag.Karta = kartas;
            return View();
        }



        #region Manifestacija
        [HttpPost]
        public ActionResult PretragaManifestacije(string Naziv, string MestoOdrzavanja, string Ulica)
        {

            DateTime DatumVremeOdrzavanjaPocetak = new DateTime();

            DateTime DatumVremeOdrzavanjaKraj = new DateTime();
            int CenaKarteMin;
            int CenaKarteMax;


            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Kupac)
            {
                //odjavljen
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Manifestacija> aktivnaListaManifestacija = new List<Manifestacija>();


            foreach (var manifestacija in manifestacijas)
            {
                if (manifestacija.Status == true)
                {
                    aktivnaListaManifestacija.Add(manifestacija);
                }
            }

            List<Manifestacija> manifestacija1 = null;
            List<Manifestacija> manifestacija2 = new List<Manifestacija>();

            if (Naziv != "")
            {
                manifestacija1 = new List<Manifestacija>();
                foreach (var manifestacija in aktivnaListaManifestacija)
                {
                    if (manifestacija.Naziv.ToLower() == Naziv.ToLower())
                    {
                        manifestacija1.Add(manifestacija);
                    }
                }
            }

            if (MestoOdrzavanja != "")
            {
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in aktivnaListaManifestacija)
                    {

                        if (manifestacija.MestoOdrzavanja.MestoOdrzavanja.Mesto.ToLower() == MestoOdrzavanja.ToLower())
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.MestoOdrzavanja.MestoOdrzavanja.Mesto.ToLower() == MestoOdrzavanja.ToLower())
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Ulica != "")
            {
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in aktivnaListaManifestacija)
                    {

                        if (manifestacija.MestoOdrzavanja.MestoOdrzavanja.Ulica.ToLower() == Ulica.ToLower())
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.MestoOdrzavanja.MestoOdrzavanja.Ulica.ToLower() == Ulica.ToLower())
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Request["DatumVremeOdrzavanjaPocetak"] != "")
            {
                DatumVremeOdrzavanjaPocetak = DateTime.Parse(Request["DatumVremeOdrzavanjaPocetak"]);
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in aktivnaListaManifestacija)
                    {

                        if (manifestacija.DatumVremeOdrzavanja >= DatumVremeOdrzavanjaPocetak)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.DatumVremeOdrzavanja >= DatumVremeOdrzavanjaPocetak)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Request["DatumVremeOdrzavanjaKraj"] != "")
            {
                DatumVremeOdrzavanjaKraj = DateTime.Parse(Request["DatumVremeOdrzavanjaKraj"]);
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in aktivnaListaManifestacija)
                    {

                        if (manifestacija.DatumVremeOdrzavanja <= DatumVremeOdrzavanjaKraj)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.DatumVremeOdrzavanja <= DatumVremeOdrzavanjaKraj)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Request["CenaKarteMin"] != "" && Request["CenaKarteMin"] != null)
            {
                CenaKarteMin = Int32.Parse(Request["CenaKarteMin"]);
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in aktivnaListaManifestacija)
                    {

                        if (manifestacija.CenaKarte >= CenaKarteMin)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.CenaKarte >= CenaKarteMin)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Request["CenaKarteMax"] != "" && Request["CenaKarteMax"] != null)
            {
                CenaKarteMax = Int32.Parse(Request["CenaKarteMax"]);
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in aktivnaListaManifestacija)
                    {

                        if (manifestacija.CenaKarte <= CenaKarteMax)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.CenaKarte <= CenaKarteMax)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }
            if (manifestacija1 == null)
            {
                manifestacija1 = aktivnaListaManifestacija;
            }
            ViewBag.manifestacije = manifestacija1;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeNazivManifestacijeOpadajuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Naziv.CompareTo(lista[j].Naziv) < 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeNazivManifestacijeRastuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Naziv.CompareTo(lista[j].Naziv) > 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeCenaKarteOpadajuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];

            List<Manifestacija> lista2 = new List<Manifestacija>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].CenaKarte.CompareTo(lista[j].CenaKarte) < 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeCenaKarteRastuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];

            List<Manifestacija> lista2 = new List<Manifestacija>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].CenaKarte.CompareTo(lista[j].CenaKarte) > 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }
        [HttpGet]
        public ActionResult SortiranjeDatumVremeOdrzavanjaOpadajuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];

            List<Manifestacija> lista2 = new List<Manifestacija>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].DatumVremeOdrzavanja.CompareTo(lista[j].DatumVremeOdrzavanja) < 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeDatumVremeOdrzavanjaRastuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];

            List<Manifestacija> lista2 = new List<Manifestacija>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].DatumVremeOdrzavanja.CompareTo(lista[j].DatumVremeOdrzavanja) > 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }


        [HttpGet]
        public ActionResult SortiranjeMestoOdrzavanjaOpadajuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].MestoOdrzavanja.MestoOdrzavanja.Mesto.CompareTo(lista[j].MestoOdrzavanja.MestoOdrzavanja.Mesto) < 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeMestoOdrzavanjaRastuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];
            

            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].MestoOdrzavanja.MestoOdrzavanja.Mesto.CompareTo(lista[j].MestoOdrzavanja.MestoOdrzavanja.Mesto) > 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }
        [HttpPost]
        public ActionResult FiltriranjeManifestacije(TipManifestacije tipManifestacije)
        {
            
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Kupac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }

            
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Manifestacija> lista = new List<Manifestacija>();

            foreach (var manifestacija in manifestacijas)
            {
                if (manifestacija.Status == true)
                {
                    lista.Add(manifestacija);
                }
            }
            

            List<Manifestacija> manifestacija1 = new List<Manifestacija>();


            foreach (var manifestacija in lista)
            {
                if (manifestacija.TipManifestacije == tipManifestacije)
                {

                    manifestacija1.Add(manifestacija);
                }
            }

            Session["manifestacije"] = manifestacija1;
            
            ViewBag.manifestacije = manifestacija1;


            return View("Index");


        }

        [HttpPost]
        public ActionResult FiltriranjeNerasprodatihManifestacija()
        {
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Kupac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }
            
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Manifestacija> lista = new List<Manifestacija>();

            foreach (var manifestacija in manifestacijas)
            {
                if (manifestacija.Status == true)
                {
                    lista.Add(manifestacija);
                }
            }

            List<Manifestacija> manifestacija1 = new List<Manifestacija>();


            foreach (var manifestacija in lista)
            {
                if (manifestacija.BrojSlobodnihMesta > 0)
                {

                    manifestacija1.Add(manifestacija);
                }
            }

            Session["manifestacije"] = manifestacija1;
            ViewBag.manifestacije = manifestacija1;


            return View("Index");

        }
        #endregion

        #region Karta
        [HttpPost]
        public ActionResult PretragaKarata(string Naziv)
        {

            DateTime DatumVremeOdrzavanjaPocetak = new DateTime();

            DateTime DatumVremeOdrzavanjaKraj = new DateTime();
            int CenaKarteMin;
            int CenaKarteMax;


            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Kupac)
            {
                //odjavljen
                Session["ulogovan"] = null;
                return View("Error");
            }

            List<Karta> kartass = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> kartas = new List<Karta>();

            foreach (var item in kartass)
            {
                if (((Korisnik)Session["ulogovan"]).KorisnickoIme == item.KorisnickoIme && item.Status == false)
                {
                    kartas.Add(item);
                }
            }

            List<Karta> karta1 = null;
            List<Karta> karta2 = new List<Karta>();

            if (Naziv != "")
            {
                karta1 = new List<Karta>();
                foreach (var karta in kartas)
                {
                    if (karta.Manifestacija.Naziv.ToLower() == Naziv.ToLower())
                    {
                        karta1.Add(karta);
                    }
                }
            }


            if (Request["DatumVremeOdrzavanjaPocetak"] != "")
            {
                DatumVremeOdrzavanjaPocetak = DateTime.Parse(Request["DatumVremeOdrzavanjaPocetak"]);
                if (karta1 == null)
                {
                    karta1 = new List<Karta>();
                    foreach (var karta in kartas)
                    {

                        if (karta.DatumOdrzavanja >= DatumVremeOdrzavanjaPocetak)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
                else
                {

                    karta2 = karta1;
                    karta1 = new List<Karta>();

                    foreach (var karta in karta2)
                    {

                        if (karta.DatumOdrzavanja >= DatumVremeOdrzavanjaPocetak)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
            }

            if (Request["DatumVremeOdrzavanjaKraj"] != "")
            {
                DatumVremeOdrzavanjaKraj = DateTime.Parse(Request["DatumVremeOdrzavanjaKraj"]);
                if (karta1 == null)
                {
                    karta1 = new List<Karta>();
                    foreach (var karta in kartas)
                    {

                        if (karta.DatumOdrzavanja <= DatumVremeOdrzavanjaKraj)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
                else
                {

                    karta2 = karta1;
                    karta1 = new List<Karta>();

                    foreach (var karta in karta2)
                    {

                        if (karta.DatumOdrzavanja <= DatumVremeOdrzavanjaKraj)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
            }

            if (Request["CenaKarteMin"] != "" && Request["CenaKarteMin"] != null)
            {
                CenaKarteMin = Int32.Parse(Request["CenaKarteMin"]);
                if (karta1 == null)
                {
                    karta1 = new List<Karta>();
                    foreach (var karta in kartas)
                    {

                        if (karta.Cena >= CenaKarteMin)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
                else
                {

                    karta2 = karta1;
                    karta1 = new List<Karta>();

                    foreach (var karta in kartas)
                    {

                        if (karta.Cena >= CenaKarteMin)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
            }

            if (Request["CenaKarteMax"] != "" && Request["CenaKarteMax"] != null)
            {
                CenaKarteMax = Int32.Parse(Request["CenaKarteMax"]);
                if (karta1 == null)
                {
                    karta1 = new List<Karta>();
                    foreach (var karta in kartas)
                    {

                        if (karta.Cena <= CenaKarteMax)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
                else
                {

                    karta2 = karta1;
                    karta1 = new List<Karta>();

                    foreach (var karta in karta2)
                    {

                        if (karta.Cena <= CenaKarteMax)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
            }
            if (karta1 == null)
            {
                karta1 = kartas;
            }
            ViewBag.Karta = karta1;

            return View("Karte");


        }

        [HttpPost]
        public ActionResult FiltriranjeTipKarte(Tip TipKarte)
        {

            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Kupac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


           List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();


            foreach (var karta in kartas)
            {
                if(((Korisnik)Session["ulogovan"]).KorisnickoIme == karta.KorisnickoIme)
                lista.Add(karta);

            }
            

            List<Karta> karta1 = new List<Karta>();


            foreach (var karta in lista)
            {
                if (karta.Tip == TipKarte)
                {
                    karta1.Add(karta);
                }
            }


            Session["karte"] = karta1;


            //slanje na front sa back-a
            ViewBag.Karta = karta1;


            return View("Karte");
        }

        [HttpPost]
        public ActionResult FiltriranjeStatusKarte(string StatusKarte)
        {
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Kupac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


            List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();

            bool rezervisana;
            List<Karta> karta1 = new List<Karta>();


            foreach (var kor in kartas)
            {
                if (((Korisnik)(Session["ulogovan"])).KorisnickoIme == kor.KorisnickoIme)
                {
                    lista.Add(kor);

                }
            }
            if (StatusKarte == "Rezervisana")
                rezervisana = false;
            else
                rezervisana = true;




            foreach (var karta in lista)
            {
                if (karta.Status == rezervisana)
                {
                    karta1.Add(karta);
                }
            }
            Session["karte"] = karta1;

            ViewBag.Karta = karta1;


            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeNazivOpadajuce()
        {
            List<Karta> listaa = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();

            foreach (var item in listaa)
            {
                if (((Korisnik)Session["ulogovan"]).KorisnickoIme == item.KorisnickoIme && item.Status == false)
                {
                    lista.Add(item);
                }
            }



            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Manifestacija.Naziv.CompareTo(lista[j].Manifestacija.Naziv) < 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Karta = lista;
            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeNazivRastuce()
        {
            List<Karta> listaa = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();

            foreach (var item in listaa)
            {
                if (((Korisnik)Session["ulogovan"]).KorisnickoIme == item.KorisnickoIme && item.Status == false)
                {
                    lista.Add(item);
                }
            }



            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Manifestacija.Naziv.CompareTo(lista[j].Manifestacija.Naziv) > 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Karta = lista;
            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeVremeOpadajuce()
        {
            List<Karta> listaa = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();

            foreach (var item in listaa)
            {
                if (((Korisnik)Session["ulogovan"]).KorisnickoIme == item.KorisnickoIme && item.Status == false)
                {
                    lista.Add(item);
                }
            }



            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].DatumOdrzavanja.CompareTo(lista[j].DatumOdrzavanja) < 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Karta = lista;

            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeVremeRastuce()
        {
            List<Karta> listaa = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();

            foreach (var item in listaa)
            {
                if (((Korisnik)Session["ulogovan"]).KorisnickoIme == item.KorisnickoIme && item.Status == false)
                {
                    lista.Add(item);
                }
            }



            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].DatumOdrzavanja.CompareTo(lista[j].DatumOdrzavanja) > 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Karta = lista;

            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeCenaOpadajuce()
        {
            List<Karta> listaa = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();

            foreach (var item in listaa)
            {
                if (((Korisnik)Session["ulogovan"]).KorisnickoIme == item.KorisnickoIme && item.Status == false)
                {
                    lista.Add(item);
                }
            }



            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Cena.CompareTo(lista[j].Cena) < 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Karta = lista;

            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeCenaRastuce()
        {
            List<Karta> listaa = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();

            foreach (var item in listaa)
            {
                if (((Korisnik)Session["ulogovan"]).KorisnickoIme == item.KorisnickoIme && item.Status == false)
                {
                    lista.Add(item);
                }
            }



            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Cena.CompareTo(lista[j].Cena) > 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Karta = lista;

            return View("Karte");
        }

        [HttpPost]
        public ActionResult DetaljiKarte(string Id)
        {

            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Kupac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }

            List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);

            Karta karta = null;

            foreach (var kartaa in kartas)
            {
                if (kartaa.JedinstveniIdentifikatorKarte == Id)
                {
                    karta = new Karta();
                    karta = kartaa;
                }

            }

            ViewBag.Karta = karta;
            return View("DetaljiKarte");
        }
        [HttpPost]
        public ActionResult DetaljiRezervisaneKarte(string Id)
        {

            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Kupac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }

            List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);

            Karta karta = null;

            foreach (var kartaa in kartas)
            {
                if (kartaa.JedinstveniIdentifikatorKarte == Id)
                {
                    karta = new Karta();
                    karta = kartaa;
                }

            }

            ViewBag.Karta = karta;
            return View("DetaljiRezervisaneKarte");
        }

        [HttpPost]
        public ActionResult OdustanakOdKarte(string Id)
        {

            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Kupac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }

            List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);
            Dictionary<string, Korisnik> k = (Dictionary<string, Korisnik>)(HttpContext.Cache["korisnici"]);
            Karta karta = new Karta();

            foreach (var kartaa in kartas)
            {
                if (kartaa.JedinstveniIdentifikatorKarte == Id)
                {
                    if ((kartaa.DatumOdrzavanja - DateTime.Now).Days > 7)
                    {
                        karta = kartaa;
                        k[((Korisnik)Session["ulogovan"]).KorisnickoIme].BrojSakupljenihBodova -= (Int32)(karta.Cena / 1000 * 133 * 4);
                        karta.Status = true;
                    }
                    else
                    {
                        List<Karta> listaKarata = ((Korisnik)(Session["ulogovan"])).SveKarte;
                        List<Karta> kartass = new List<Karta>();

                        foreach (var kartaaa in listaKarata)
                        {
                            if (kartaaa.Status == false)
                            {
                                kartass.Add(kartaaa);
                            }
                        }
                        ViewBag.Karta = kartass;
                        ViewBag.Error = true;
                        return View("Karte");
                    }


                }

            }
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);

            foreach (var manifestacija in manifestacijas)
            {
                if (manifestacija.Id == karta.Manifestacija.Id)
                {
                    manifestacija.BrojSlobodnihMesta += karta.BrojKarata;
                }
            }
            baza.ListaKarata(kartas);
            baza.ListaManifestacija(manifestacijas);
            baza.ListaKorisnika(k);
            HttpContext.Cache["manifestacije"] = manifestacijas;
            HttpContext.Cache["karte"] = kartas;
            HttpContext.Cache["korisnici"] = k;
            Session["ulogovan"] = k[((Korisnik)Session["ulogovan"]).KorisnickoIme];

            return RedirectToAction("Karte");
        }
        #endregion

        #region Komentar
        
        [HttpPost]
        public ActionResult DodavanjeKomentara(int Id)
        {
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Kupac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }

            ViewBag.Id = Id;

            return View("DodajKomentar");
        }

        [HttpPost]
        public ActionResult DodajKomentar(string TekstKomentara, int Ocena, int Id)
        {

            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Kupac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }

            if(TekstKomentara == "")
            {
                ViewBag.Id = Id;
                ViewBag.Error = "Greska - Tekst komentara ne moze biti prazan";
                return View();
            }

            List<Komentar> komentars = (List<Komentar>)(HttpContext.Cache["komentari"]);
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            
            foreach(var manifestacija in manifestacijas)
            {
                if(manifestacija.Id == Id)
                {
                    manifestacija.ProsecnaOcena = (manifestacija.ProsecnaOcena * manifestacija.BrojKomentara + Ocena)/(1+manifestacija.BrojKomentara) ;
                    manifestacija.BrojKomentara++;
                }
            }


            Korisnik korisnik = (Korisnik)Session["ulogovan"];

            Komentar komentar = new Komentar();

            
            komentar.TekstKomentara = TekstKomentara;
            komentar.Ocena = Ocena;
            komentar.Kupac = korisnik.KorisnickoIme;
            komentar.Manifestacija = Id;
            int max = 0;

            foreach(var komentarr in komentars)
            {
                if(komentarr.Id > max)
                {
                    max = komentarr.Id;
                }
            }
            komentar.Id = max + 1;
            komentars.Add(komentar);
            
            baza.ListaKomentara(komentars);
            HttpContext.Cache["komentari"] = komentars;
            baza.ListaManifestacija(manifestacijas);
            HttpContext.Cache["manifestacije"] = manifestacijas;
            return RedirectToAction("Index");
        }

        #endregion
    }
}