﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web1_Projekat.Controllers
{
    public class ProdavacController : Controller
    {
        Baza baza = new Baza();
        // GET: Prodavac
        public ActionResult Index()
        {

            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Prodavac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }

            
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Manifestacija> listaManifestacijaProdavca = new List<Manifestacija>();

            foreach (var item in manifestacijas)
            {
                if (item.Prodavac == ((Korisnik)Session["ulogovan"]).KorisnickoIme)
                {

                    listaManifestacijaProdavca.Add(item);
                }
            }

            ViewBag.manifestacije = listaManifestacijaProdavca;
            Session["manifestacije"] = listaManifestacijaProdavca;

            return View();
        }

        #region Manifestacija
        [HttpGet]
        public ActionResult Manifestacija()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NapraviManifestaciju(Manifestacija manifestacija, HttpPostedFileBase file)
        {
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Lokacija> lokacijas = (List<Lokacija>)(HttpContext.Cache["lokacije"]);

            if (Request["TipManifestacije"] == "1")
            {
                manifestacija.TipManifestacije = TipManifestacije.Koncert;
            }
            else if (Request["TipManifestacije"] == "2")
            {
                manifestacija.TipManifestacije = TipManifestacije.Festival;
            }
            else
            {
                manifestacija.TipManifestacije = TipManifestacije.Pozoriste;
            }

            manifestacija.MestoOdrzavanja.MestoOdrzavanja.Ulica = Request["Ulica"];
            manifestacija.MestoOdrzavanja.MestoOdrzavanja.Broj = Int32.Parse(Request["Broj"]);
            manifestacija.MestoOdrzavanja.MestoOdrzavanja.Mesto = Request["Mesto"];
            manifestacija.MestoOdrzavanja.MestoOdrzavanja.PostanskiBroj = Int32.Parse(Request["PostanskiBroj"]);

            manifestacija.MestoOdrzavanja.GeografskaDuzina = Request["GeografskaDuzina"];
            manifestacija.MestoOdrzavanja.GeografskaSirina = Request["GeografskaSirina"];
            manifestacija.BrojSlobodnihMesta = manifestacija.BrojMesta;

            int maxId = 0;


            bool postoji = true;

            foreach (var item in manifestacijas)
            {
                if (item.DatumVremeOdrzavanja == manifestacija.DatumVremeOdrzavanja && item.MestoOdrzavanja.MestoOdrzavanja.Mesto.ToLower() == manifestacija.MestoOdrzavanja.MestoOdrzavanja.Mesto.ToLower() && item.MestoOdrzavanja.MestoOdrzavanja.Broj == manifestacija.MestoOdrzavanja.MestoOdrzavanja.Broj && item.MestoOdrzavanja.MestoOdrzavanja.Ulica.ToLower() == manifestacija.MestoOdrzavanja.MestoOdrzavanja.Ulica.ToLower())
                {
                    postoji = false;

                }

                if (item.Id > maxId)
                {
                    maxId = item.Id;

                }


            }

            if (file != null)
            {
                string pic = System.IO.Path.GetFileName(file.FileName);
                if (manifestacija.PosterManifestacije != "")
                {
                    string path = System.IO.Path.Combine(Server.MapPath("~/Poster"), pic);
                    file.SaveAs(path);
                    manifestacija.PosterManifestacije = ("/Poster/" + pic);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        file.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                    }
                }
            }

            if (postoji)
            {
                manifestacija.Prodavac = ((Korisnik)Session["ulogovan"]).KorisnickoIme;

                manifestacija.Id = maxId + 1;
                manifestacija.MestoOdrzavanja.Manifestacija = manifestacija.Id;
                manifestacijas.Add(manifestacija);
                lokacijas.Add(manifestacija.MestoOdrzavanja);

                HttpContext.Cache["manifestacije"] = manifestacijas;
                HttpContext.Cache["lokacije"] = lokacijas;
                baza.ListaLokacija(lokacijas);
                baza.ListaManifestacija(manifestacijas);
            }

            return RedirectToAction("Index");
        }


        [HttpPost]
        public ActionResult PrikazManifestacije(int id)
        {

            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Komentar> komentars = (List<Komentar>)(HttpContext.Cache["komentari"]);
            List<Komentar> sviKomentari = new List<Komentar>();
            foreach (var item in manifestacijas)
            {
                if (id == item.Id)
                {
                    ViewBag.Manifestacija = item;
                }
            }

            foreach(var item in komentars)
            {
                if(item.Manifestacija == id)
                {
                    sviKomentari.Add(item);
                }
            }

            ViewBag.komentari = sviKomentari;

            return View();
        }

        [HttpGet]
        public ActionResult IzmenaManifestacije(int id)
        {

            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);

            foreach (var item in manifestacijas)
            {
                if (id == item.Id)
                {
                    ViewBag.Manifestacija = item;
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult SacuvajIzmenuManifestacije(Manifestacija manifestacija)
        {
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Lokacija> lokacijas = (List<Lokacija>)(HttpContext.Cache["lokacije"]);


            if (Request["TipManifestacije"] == "1")
            {
                manifestacija.TipManifestacije = TipManifestacije.Koncert;
            }
            else if (Request["TipManifestacije"] == "2")
            {
                manifestacija.TipManifestacije = TipManifestacije.Festival;
            }
            else
            {
                manifestacija.TipManifestacije = TipManifestacije.Pozoriste;
            }

            manifestacija.MestoOdrzavanja.MestoOdrzavanja.Ulica = Request["Ulica"];
            manifestacija.MestoOdrzavanja.MestoOdrzavanja.Broj = Int32.Parse(Request["Broj"]);
            manifestacija.MestoOdrzavanja.MestoOdrzavanja.Mesto = Request["Mesto"];
            manifestacija.MestoOdrzavanja.MestoOdrzavanja.PostanskiBroj = Int32.Parse(Request["PostanskiBroj"]);

            manifestacija.MestoOdrzavanja.GeografskaDuzina = Request["GeografskaDuzina"];
            manifestacija.MestoOdrzavanja.GeografskaSirina = Request["GeografskaSirina"];

            manifestacija.MestoOdrzavanja.Manifestacija = manifestacija.Id;
          
            bool postoji = true;

            foreach (var item in manifestacijas)
            {

                if (item.Id != manifestacija.Id && item.DatumVremeOdrzavanja == manifestacija.DatumVremeOdrzavanja && item.MestoOdrzavanja.MestoOdrzavanja.Mesto.ToLower() == manifestacija.MestoOdrzavanja.MestoOdrzavanja.Mesto.ToLower() && item.MestoOdrzavanja.MestoOdrzavanja.Broj == manifestacija.MestoOdrzavanja.MestoOdrzavanja.Broj && item.MestoOdrzavanja.MestoOdrzavanja.Ulica.ToLower() == manifestacija.MestoOdrzavanja.MestoOdrzavanja.Ulica.ToLower())
                {

                    postoji = false;

                }
            }

            if (postoji)
            {
                int index = 0;
                for (int i = 0; i < manifestacijas.Count; i++)
                {
                    if (manifestacijas[i].Id == manifestacija.Id)
                    {
                        index = i;
                        manifestacija.PosterManifestacije = manifestacijas[i].PosterManifestacije;
                    }
                }


                int index1 = 0;
                for (int i = 0; i < lokacijas.Count; i++)
                {
                    if (lokacijas[i].Manifestacija == manifestacija.Id)
                    {
                        index1 = i;
                    }
                }

                manifestacijas.RemoveAt(index);
                lokacijas.RemoveAt(index1);

                manifestacija.Prodavac = ((Korisnik)Session["ulogovan"]).KorisnickoIme;

                manifestacijas.Add(manifestacija);
                lokacijas.Add(manifestacija.MestoOdrzavanja);

                HttpContext.Cache["manifestacije"] = manifestacijas;
                HttpContext.Cache["lokacije"] = lokacijas;
                baza.ListaLokacija(lokacijas);
                baza.ListaManifestacija(manifestacijas);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult PretragaManifestacije(string Naziv, string MestoOdrzavanja, string Ulica)
        {

            DateTime DatumVremeOdrzavanjaPocetak = new DateTime();

            DateTime DatumVremeOdrzavanjaKraj = new DateTime();
            int CenaKarteMin;
            int CenaKarteMax;


            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Prodavac)
            {
                //odjavljen
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Manifestacija> listaManifestacijaProdavca = new List<Manifestacija>();


            foreach (var item in manifestacijas)
            {
                if (item.Prodavac == ((Korisnik)Session["ulogovan"]).KorisnickoIme)
                {

                    listaManifestacijaProdavca.Add(item);
                }
            }

            manifestacijas = listaManifestacijaProdavca;


            List<Manifestacija> manifestacija1 = null;
            List<Manifestacija> manifestacija2 = new List<Manifestacija>();

            if (Naziv != "")
            {
                manifestacija1 = new List<Manifestacija>();
                foreach (var manifestacija in manifestacijas)
                {
                    if (manifestacija.Naziv.ToLower() == Naziv.ToLower())
                    {
                        manifestacija1.Add(manifestacija);
                    }
                }
            }

            if (MestoOdrzavanja != "")
            {
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in manifestacijas)
                    {

                        if (manifestacija.MestoOdrzavanja.MestoOdrzavanja.Mesto.ToLower() == MestoOdrzavanja.ToLower())
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.MestoOdrzavanja.MestoOdrzavanja.Mesto.ToLower() == MestoOdrzavanja.ToLower())
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Ulica != "")
            {
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in manifestacijas)
                    {

                        if (manifestacija.MestoOdrzavanja.MestoOdrzavanja.Ulica.ToLower() == Ulica.ToLower())
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.MestoOdrzavanja.MestoOdrzavanja.Ulica.ToLower() == Ulica.ToLower())
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Request["DatumVremeOdrzavanjaPocetak"] != "")
            {
                DatumVremeOdrzavanjaPocetak = DateTime.Parse(Request["DatumVremeOdrzavanjaPocetak"]);
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in manifestacijas)
                    {

                        if (manifestacija.DatumVremeOdrzavanja >= DatumVremeOdrzavanjaPocetak)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.DatumVremeOdrzavanja >= DatumVremeOdrzavanjaPocetak)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Request["DatumVremeOdrzavanjaKraj"] != "")
            {
                DatumVremeOdrzavanjaKraj = DateTime.Parse(Request["DatumVremeOdrzavanjaKraj"]);
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in manifestacijas)
                    {

                        if (manifestacija.DatumVremeOdrzavanja <= DatumVremeOdrzavanjaKraj)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.DatumVremeOdrzavanja <= DatumVremeOdrzavanjaKraj)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Request["CenaKarteMin"] != "" && Request["CenaKarteMin"] != null)
            {
                CenaKarteMin = Int32.Parse(Request["CenaKarteMin"]);
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in manifestacijas)
                    {

                        if (manifestacija.CenaKarte >= CenaKarteMin)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.CenaKarte >= CenaKarteMin)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Request["CenaKarteMax"] != "" && Request["CenaKarteMax"] != null)
            {
                CenaKarteMax = Int32.Parse(Request["CenaKarteMax"]);
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in manifestacijas)
                    {

                        if (manifestacija.CenaKarte <= CenaKarteMax)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.CenaKarte <= CenaKarteMax)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }
            if (manifestacija1 == null)
            {
                manifestacija1 = manifestacijas;
            }
            ViewBag.manifestacije = manifestacija1;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeNazivManifestacijeOpadajuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Naziv.CompareTo(lista[j].Naziv) < 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeNazivManifestacijeRastuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Naziv.CompareTo(lista[j].Naziv) > 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeCenaKarteOpadajuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];

            List<Manifestacija> lista2 = new List<Manifestacija>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].CenaKarte.CompareTo(lista[j].CenaKarte) < 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeCenaKarteRastuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];

            List<Manifestacija> lista2 = new List<Manifestacija>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].CenaKarte.CompareTo(lista[j].CenaKarte) > 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeDatumVremeOdrzavanjaOpadajuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];

            List<Manifestacija> lista2 = new List<Manifestacija>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].DatumVremeOdrzavanja.CompareTo(lista[j].DatumVremeOdrzavanja) < 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeDatumVremeOdrzavanjaRastuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];

            List<Manifestacija> lista2 = new List<Manifestacija>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].DatumVremeOdrzavanja.CompareTo(lista[j].DatumVremeOdrzavanja) > 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }


        [HttpGet]
        public ActionResult SortiranjeMestoOdrzavanjaOpadajuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].MestoOdrzavanja.MestoOdrzavanja.Mesto.CompareTo(lista[j].MestoOdrzavanja.MestoOdrzavanja.Mesto) < 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeMestoOdrzavanjaRastuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)Session["manifestacije"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].MestoOdrzavanja.MestoOdrzavanja.Mesto.CompareTo(lista[j].MestoOdrzavanja.MestoOdrzavanja.Mesto) > 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Index");
        }


        [HttpPost]
        public ActionResult FiltriranjeManifestacije(TipManifestacije tipManifestacije)
        {

            //ako necega nema, izbaci gresku
            //fali filter za ono drugo
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Prodavac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Manifestacija> lista = new List<Manifestacija>();


            foreach (var manifestacija in manifestacijas)
            {
                
                lista.Add(manifestacija);

            }

            List<Manifestacija> manifestacija1 = new List<Manifestacija>();


            foreach (var manifestacija in lista)
            {
                if (manifestacija.TipManifestacije == tipManifestacije)
                {

                    manifestacija1.Add(manifestacija);
                }
            }

            Session["manifestacije"] = manifestacija1;


            //slanje na front sa back-a
            ViewBag.manifestacije = manifestacija1;


            return View("Index");


        }

        [HttpPost]
        public ActionResult FiltriranjeNerasprodatihManifestacija()
        {
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Prodavac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Manifestacija> lista = new List<Manifestacija>();


            foreach (var manifestacija in manifestacijas)
            {
                lista.Add(manifestacija);

            }



            List<Manifestacija> manifestacija1 = new List<Manifestacija>();


            foreach (var manifestacija in lista)
            {
                if (manifestacija.BrojSlobodnihMesta > 0)
                {

                    manifestacija1.Add(manifestacija);
                }
            }

            Session["manifestacije"] = manifestacija1;


            //slanje na front sa back-a
            ViewBag.manifestacije = manifestacija1;


            return View("Index");

        }
        #endregion

        #region Karta
        [HttpGet]
        public ActionResult Karte()
        {

            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Prodavac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


            List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> listaRezervisanihKarata = new List<Karta>();

            foreach(var karta in kartas)
            {
                if (karta.Status == false)
                    listaRezervisanihKarata.Add(karta);
            }
            ViewBag.Karta = listaRezervisanihKarata;
            return View("Karte");
        }
        
        [HttpPost]
        public ActionResult DetaljiKarte(string Id)
        {
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Prodavac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }
            
            List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);

            Karta karta = null;

            foreach (var kartaa in kartas)
            {
                if (kartaa.JedinstveniIdentifikatorKarte == Id)
                {
                    karta = new Karta();
                    karta = kartaa;
                }
                    
            }

            ViewBag.Karta = karta;
            return View("DetaljiKarte");
        }

        [HttpPost]
        public ActionResult PretragaKarata(string Naziv)
        {

            DateTime DatumVremeOdrzavanjaPocetak = new DateTime();

            DateTime DatumVremeOdrzavanjaKraj = new DateTime();
            int CenaKarteMin;
            int CenaKarteMax;


            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Prodavac)
            {
                //odjavljen
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            List<Karta> kartass = (List<Karta>)(HttpContext.Cache["karte"]);

            List<Karta> kartas = new List<Karta>();

            foreach (var item in kartass)
            {
                if (item.Status == false)
                {
                    kartas.Add(item);
                }
            }

            List<Karta> karta1 = null;
            List<Karta> karta2 = new List<Karta>();

            if (Naziv != "")
            {
                karta1 = new List<Karta>();
                foreach (var karta in kartas)
                {
                    if (karta.Manifestacija.Naziv.ToLower() == Naziv.ToLower())
                    {
                        karta1.Add(karta);
                    }
                }
            }


            if (Request["DatumVremeOdrzavanjaPocetak"] != "")
            {
                DatumVremeOdrzavanjaPocetak = DateTime.Parse(Request["DatumVremeOdrzavanjaPocetak"]);
                if (karta1 == null)
                {
                    karta1 = new List<Karta>();
                    foreach (var karta in kartas)
                    {

                        if (karta.DatumOdrzavanja >= DatumVremeOdrzavanjaPocetak)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
                else
                {

                    karta2 = karta1;
                    karta1 = new List<Karta>();

                    foreach (var karta in karta2)
                    {

                        if (karta.DatumOdrzavanja >= DatumVremeOdrzavanjaPocetak)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
            }

            if (Request["DatumVremeOdrzavanjaKraj"] != "")
            {
                DatumVremeOdrzavanjaKraj = DateTime.Parse(Request["DatumVremeOdrzavanjaKraj"]);
                if (karta1 == null)
                {
                    karta1 = new List<Karta>();
                    foreach (var karta in kartas)
                    {

                        if (karta.DatumOdrzavanja <= DatumVremeOdrzavanjaKraj)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
                else
                {

                    karta2 = karta1;
                    karta1 = new List<Karta>();

                    foreach (var karta in karta2)
                    {

                        if (karta.DatumOdrzavanja <= DatumVremeOdrzavanjaKraj)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
            }

            if (Request["CenaKarteMin"] != "" && Request["CenaKarteMin"] != null)
            {
                CenaKarteMin = Int32.Parse(Request["CenaKarteMin"]);
                if (karta1 == null)
                {
                    karta1 = new List<Karta>();
                    foreach (var karta in kartas)
                    {

                        if (karta.Cena >= CenaKarteMin)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
                else
                {

                    karta2 = karta1;
                    karta1 = new List<Karta>();

                    foreach (var karta in kartas)
                    {

                        if (karta.Cena >= CenaKarteMin)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
            }

            if (Request["CenaKarteMax"] != "" && Request["CenaKarteMax"] != null)
            {
                CenaKarteMax = Int32.Parse(Request["CenaKarteMax"]);
                if (karta1 == null)
                {
                    karta1 = new List<Karta>();
                    foreach (var karta in kartas)
                    {

                        if (karta.Cena <= CenaKarteMax)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
                else
                {

                    karta2 = karta1;
                    karta1 = new List<Karta>();

                    foreach (var karta in karta2)
                    {

                        if (karta.Cena <= CenaKarteMax)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
            }
            if (karta1 == null)
            {
                karta1 = kartas;
            }
            ViewBag.Karta = karta1;

            return View("Karte");


        }

        [HttpPost]
        public ActionResult FiltriranjeTipKarte(Tip TipKarte)
        {

            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Prodavac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


            List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();


            foreach (var karta in kartas)
            {
                if(karta.Status == false)
                lista.Add(karta);

            }
            

            List<Karta> karta1 = new List<Karta>();


            foreach (var karta in lista)
            {
                if (karta.Tip == TipKarte)
                {
                    karta1.Add(karta);
                }
            }


            Session["karte"] = karta1;


            //slanje na front sa back-a
            ViewBag.Karta = karta1;


            return View("Karte");
        }

        [HttpPost]
        public ActionResult FiltriranjeStatusKarte(string StatusKarte)
        {
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Prodavac)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


            List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();

            bool rezervisana;

            if (StatusKarte == "Rezervisana")
                rezervisana = false;
            else
                rezervisana = true;

            foreach (var karta in kartas)
            {
                lista.Add(karta);

            }

            List<Karta> karta1 = new List<Karta>();


            foreach (var karta in lista)
            {
                if (karta.Status == rezervisana)
                {
                    karta1.Add(karta);
                }
            }


            Session["karte"] = karta1;


            //slanje na front sa back-a
            ViewBag.Karta = karta1;


            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeNazivOpadajuce()
        {
            List<Karta> listaa = (List<Karta>)(HttpContext.Cache["karte"]);

            List<Karta> lista = new List<Karta>();

            foreach (var item in listaa)
            {
                if (item.Status == false)
                {
                    lista.Add(item);
                }
            }

            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Manifestacija.Naziv.CompareTo(lista[j].Manifestacija.Naziv) < 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Karta = lista;
            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeNazivRastuce()
        {
            List<Karta> listaa = (List<Karta>)(HttpContext.Cache["karte"]);

            List<Karta> lista = new List<Karta>();

            foreach (var item in listaa)
            {
                if (item.Status == false)
                {
                    lista.Add(item);
                }
            }
            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Manifestacija.Naziv.CompareTo(lista[j].Manifestacija.Naziv) > 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Karta = lista;
            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeVremeOpadajuce()
        {
            List<Karta> listaa = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();

            foreach (var item in listaa)
            {
                if (item.Status == false)
                {
                    lista.Add(item);
                }
            }

            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].DatumOdrzavanja.CompareTo(lista[j].DatumOdrzavanja) < 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Karta = lista;

            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeVremeRastuce()
        {
            List<Karta> listaa = (List<Karta>)(HttpContext.Cache["karte"]);

            List<Karta> lista = new List<Karta>();

            foreach (var item in listaa)
            {
                if (item.Status == false)
                {
                    lista.Add(item);
                }
            }

            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].DatumOdrzavanja.CompareTo(lista[j].DatumOdrzavanja) > 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Karta = lista;

            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeCenaOpadajuce()
        {
            List<Karta> listaa = (List<Karta>)(HttpContext.Cache["karte"]);

            List<Karta> lista = new List<Karta>();

            foreach (var item in listaa)
            {
                if (item.Status == false)
                {
                    lista.Add(item);
                }
            }


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Cena.CompareTo(lista[j].Cena) < 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Karta = lista;

            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeCenaRastuce()
        {
            List<Karta> listaa = (List<Karta>)(HttpContext.Cache["karte"]);

            List<Karta> lista = new List<Karta>();

            foreach (var item in listaa)
            {
                if (item.Status == false)
                {
                    lista.Add(item);
                }
            }


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Cena.CompareTo(lista[j].Cena) > 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Karta = lista;

            return View("Karte");
        }
        #endregion

        #region Komentar

        [HttpPost]
        public ActionResult AktivirajKomentar(int Id)
        {
            List<Komentar> komentars = (List<Komentar>)(HttpContext.Cache["komentari"]);
            int manifestacija = 0;
            foreach(var item in komentars)
            {
                if(item.Id == Id)
                {
                    item.Status = true;
                    manifestacija = item.Manifestacija;
                }
            }

            baza.ListaKomentara(komentars);
            HttpContext.Cache["komentari"] = komentars;
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Komentar> sviKomentari = new List<Komentar>();

            foreach (var item in manifestacijas)
            {
                if (manifestacija == item.Id)
                {
                    ViewBag.Manifestacija = item;
                }
            }

            foreach (var item in komentars)
            {
                if (item.Manifestacija == manifestacija)
                {
                    sviKomentari.Add(item);
                }
            }

            ViewBag.komentari = sviKomentari;
            return View("PrikazManifestacije");
        }

        #endregion
    }
}