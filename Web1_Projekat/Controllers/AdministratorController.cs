﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web1_Projekat.Controllers
{
    public class AdministratorController : Controller
    {
        Baza baza = new Baza();
        // GET: Administrator
        public ActionResult Index()
        {

            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                //odjavljen
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            Dictionary<string, Korisnik> listaSvihKorisnika = (Dictionary<string, Korisnik>)(HttpContext.Cache["korisnici"]);
            List<Korisnik> lista = new List<Korisnik>();


            foreach (var korisnik in listaSvihKorisnika.Values)
            {
                lista.Add(korisnik);

            }

            Session["sviKorisnici"] = lista;


            //slanje na front sa back-a
            ViewBag.korisnici = lista;

            return View();
        }

        [HttpGet]
        public ActionResult Manifestacija()
        {
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                //odjavljen
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);

            ViewBag.manifestacije = manifestacijas;

            return View();
        }

        [HttpGet]
        public ActionResult Karte()
        {

            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                //odjavljen
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);

            ViewBag.karte = kartas;
            return View();
        }

        [HttpPost]
        public ActionResult AktivacijaManifestacije(int Id)
        {

            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                //odjavljen
                Session["ulogovan"] = null;
                return View("Error");
            }
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);

            foreach (var item in manifestacijas)
            {
                if (item.Id == Id)
                {
                    item.Status = true;
                }
            }

            HttpContext.Cache["manifestacije"] = manifestacijas;
            baza.ListaManifestacija(manifestacijas);

            return RedirectToAction("Manifestacija");
        }

        #region Korisnik
        [HttpPost]
        public ActionResult PretragaKorisnika(string KorisnickoIme, string Ime, string Prezime)
        {

            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            Dictionary<string, Korisnik> listaSvihKorisnika = (Dictionary<string, Korisnik>)(HttpContext.Cache["korisnici"]);
            List<Korisnik> lista = new List<Korisnik>();


            foreach (var korisnik in listaSvihKorisnika.Values)
            {
                lista.Add(korisnik);

            }

            List<Korisnik> korisnik1 = null;
            List<Korisnik> korisnik2 = new List<Korisnik>();

            if (KorisnickoIme != "")
            {
                korisnik1 = new List<Korisnik>();
                foreach (var korisnik in lista)
                {
                    if (korisnik.KorisnickoIme == KorisnickoIme)
                    {
                        korisnik1.Add(korisnik);
                    }
                }
            }

            if (Ime != "")
            {
                if (korisnik1 == null)
                {
                    korisnik1 = new List<Korisnik>();
                    foreach (var korisnik in lista)
                    {

                        if (korisnik.Ime == Ime)
                        {
                            korisnik1.Add(korisnik);
                        }
                    }
                }
                else
                {

                    korisnik2 = korisnik1;
                    korisnik1 = new List<Korisnik>();

                    foreach (var korisnik in korisnik2)
                    {

                        if (korisnik.Ime == Ime)
                        {
                            korisnik1.Add(korisnik);
                        }
                    }
                }
            }

            if (Prezime != "")
            {
                if (korisnik1 == null)
                {
                    korisnik1 = new List<Korisnik>();
                    foreach (var korisnik in lista)
                    {

                        if (korisnik.Prezime == Prezime)
                        {
                            korisnik1.Add(korisnik);
                        }
                    }
                }
                else
                {

                    korisnik2 = korisnik1;
                    korisnik1 = new List<Korisnik>();

                    foreach (var korisnik in korisnik2)
                    {

                        if (korisnik.Prezime == Prezime)
                        {
                            korisnik1.Add(korisnik);
                        }
                    }
                }
            }

            if (KorisnickoIme == "" && Ime == "" && Prezime == "")
            {
                korisnik1 = lista;
            }

            Session["sviKorisnici"] = korisnik1;


            //slanje na front sa back-a
            ViewBag.korisnici = korisnik1;


            return View("Index");
        }

        [HttpPost]
        public ActionResult PretragaManifestacije(string Naziv, string MestoOdrzavanja, string Ulica)
        {

            DateTime DatumVremeOdrzavanjaPocetak = new DateTime();

            DateTime DatumVremeOdrzavanjaKraj = new DateTime();
            int CenaKarteMin;
            int CenaKarteMax;


            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                //odjavljen
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);


            List<Manifestacija> manifestacija1 = null;
            List<Manifestacija> manifestacija2 = new List<Manifestacija>();

            if (Naziv != "")
            {
                manifestacija1 = new List<Manifestacija>();
                foreach (var manifestacija in manifestacijas)
                {
                    if (manifestacija.Naziv.ToLower() == Naziv.ToLower())
                    {
                        manifestacija1.Add(manifestacija);
                    }
                }
            }

            if (MestoOdrzavanja != "")
            {
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in manifestacijas)
                    {

                        if (manifestacija.MestoOdrzavanja.MestoOdrzavanja.Mesto.ToLower() == MestoOdrzavanja.ToLower())
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.MestoOdrzavanja.MestoOdrzavanja.Mesto.ToLower() == MestoOdrzavanja.ToLower())
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Ulica != "")
            {
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in manifestacijas)
                    {

                        if (manifestacija.MestoOdrzavanja.MestoOdrzavanja.Ulica.ToLower() == Ulica.ToLower())
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.MestoOdrzavanja.MestoOdrzavanja.Ulica.ToLower() == Ulica.ToLower())
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Request["DatumVremeOdrzavanjaPocetak"] != "")
            {
                DatumVremeOdrzavanjaPocetak = DateTime.Parse(Request["DatumVremeOdrzavanjaPocetak"]);
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in manifestacijas)
                    {

                        if (manifestacija.DatumVremeOdrzavanja >= DatumVremeOdrzavanjaPocetak)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.DatumVremeOdrzavanja >= DatumVremeOdrzavanjaPocetak)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Request["DatumVremeOdrzavanjaKraj"] != "")
            {
                DatumVremeOdrzavanjaKraj = DateTime.Parse(Request["DatumVremeOdrzavanjaKraj"]);
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in manifestacijas)
                    {

                        if (manifestacija.DatumVremeOdrzavanja <= DatumVremeOdrzavanjaKraj)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.DatumVremeOdrzavanja <= DatumVremeOdrzavanjaKraj)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Request["CenaKarteMin"] != "" && Request["CenaKarteMin"] != null)
            {
                CenaKarteMin = Int32.Parse(Request["CenaKarteMin"]);
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in manifestacijas)
                    {

                        if (manifestacija.CenaKarte >= CenaKarteMin)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.CenaKarte >= CenaKarteMin)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }

            if (Request["CenaKarteMax"] != "" && Request["CenaKarteMax"] != null)
            {
                CenaKarteMax = Int32.Parse(Request["CenaKarteMax"]);
                if (manifestacija1 == null)
                {
                    manifestacija1 = new List<Manifestacija>();
                    foreach (var manifestacija in manifestacijas)
                    {

                        if (manifestacija.CenaKarte <= CenaKarteMax)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
                else
                {

                    manifestacija2 = manifestacija1;
                    manifestacija1 = new List<Manifestacija>();

                    foreach (var manifestacija in manifestacija2)
                    {

                        if (manifestacija.CenaKarte <= CenaKarteMax)
                        {
                            manifestacija1.Add(manifestacija);
                        }
                    }
                }
            }
            if (manifestacija1 == null)
            {
                manifestacija1 = manifestacijas;
            }
            ViewBag.manifestacije = manifestacija1;

            return View("Manifestacija");
        }

        [HttpGet]
        public ActionResult SortiranjeKorisnickoImeOpadajuce()
        {
            List<Korisnik> lista = (List<Korisnik>)Session["sviKorisnici"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].KorisnickoIme.CompareTo(lista[j].KorisnickoIme) < 0)
                    {
                        Korisnik temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Korisnici = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeKorisnickoImeRastuce()
        {
            List<Korisnik> lista = (List<Korisnik>)Session["sviKorisnici"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].KorisnickoIme.CompareTo(lista[j].KorisnickoIme) > 0)
                    {
                        Korisnik temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Korisnici = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeImeOpadajuce()
        {
            List<Korisnik> lista = (List<Korisnik>)Session["sviKorisnici"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Ime.CompareTo(lista[j].Ime) < 0)
                    {
                        Korisnik temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Korisnici = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeImeRastuce()
        {
            List<Korisnik> lista = (List<Korisnik>)Session["sviKorisnici"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Ime.CompareTo(lista[j].Ime) > 0)
                    {
                        Korisnik temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Korisnici = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjePrezimeOpadajuce()
        {
            List<Korisnik> lista = (List<Korisnik>)Session["sviKorisnici"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Prezime.CompareTo(lista[j].Prezime) < 0)
                    {
                        Korisnik temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Korisnici = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjePrezimeRastuce()
        {
            List<Korisnik> lista = (List<Korisnik>)Session["sviKorisnici"];


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Prezime.CompareTo(lista[j].Prezime) > 0)
                    {
                        Korisnik temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Korisnici = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeBrojBodovaOpadajuce()
        {
            List<Korisnik> lista = (List<Korisnik>)Session["sviKorisnici"];
            List<Korisnik> lista2 = new List<Korisnik>();

            for (int i = 0; i < lista.Count; i++)
            {
                if (lista[i].Uloga != Uloga.Kupac)
                {
                    lista2.Add(lista[i]);

                }

            }

            for (int i = 0; i < lista.Count; i++)
            {
                if (lista[i].Uloga == Uloga.Kupac)
                {
                    lista2.Add(lista[i]);

                }

            }


            lista = lista2;

            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].BrojSakupljenihBodova.CompareTo(lista[j].BrojSakupljenihBodova) < 0 && lista[i].Uloga == Uloga.Kupac && lista[j].Uloga == Uloga.Kupac)
                    {
                        Korisnik temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Korisnici = lista;

            return View("Index");
        }

        [HttpGet]
        public ActionResult SortiranjeBrojBodovaRastuce()
        {
            List<Korisnik> lista = (List<Korisnik>)Session["sviKorisnici"];

            List<Korisnik> lista2 = new List<Korisnik>();

            for (int i = 0; i < lista.Count; i++)
            {
                if (lista[i].Uloga != Uloga.Kupac)
                {
                    lista2.Add(lista[i]);

                }

            }

            for (int i = 0; i < lista.Count; i++)
            {
                if (lista[i].Uloga == Uloga.Kupac)
                {
                    lista2.Add(lista[i]);

                }

            }

            lista = lista2;

            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].BrojSakupljenihBodova.CompareTo(lista[j].BrojSakupljenihBodova) > 0 && lista[i].Uloga == Uloga.Kupac && lista[j].Uloga == Uloga.Kupac)
                    {
                        Korisnik temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.Korisnici = lista;

            return View("Index");
        }

        [HttpPost]
        public ActionResult FiltriranjeKorisnika(Uloga uloga)
        {

            //fali filter za tipKorisnika
            //izbacice gresku ako nema nekog
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            Dictionary<string, Korisnik> listaSvihKorisnika = (Dictionary<string, Korisnik>)(HttpContext.Cache["korisnici"]);
            List<Korisnik> lista = new List<Korisnik>();


            foreach (var korisnik in listaSvihKorisnika.Values)
            {
                lista.Add(korisnik);

            }

            List<Korisnik> korisnik1 = new List<Korisnik>();


            foreach (var korisnik in lista)
            {
                if (korisnik.Uloga == uloga)
                {
                    korisnik1.Add(korisnik);
                }
            }


            Session["sviKorisnici"] = korisnik1;


            //slanje na front sa back-a
            ViewBag.korisnici = korisnik1;


            return View("Index");
        }

        [HttpPost]
        public ActionResult FiltriranjeTipova(ImeTipa imeTipa)
        {

            //fali filter za tipKorisnika
            //izbacice gresku ako nema nekog
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            Dictionary<string, Korisnik> listaSvihKorisnika = (Dictionary<string, Korisnik>)(HttpContext.Cache["korisnici"]);
            List<Korisnik> lista = new List<Korisnik>();


            foreach (var korisnik in listaSvihKorisnika.Values)
            {
                lista.Add(korisnik);

            }

            List<Korisnik> korisnik1 = new List<Korisnik>();


            foreach (var korisnik in lista)
            {
                if (korisnik.Uloga == Uloga.Kupac)
                {
                    if (korisnik.TipKorisnika == imeTipa)
                    {
                        korisnik1.Add(korisnik);
                    }
                }
            }


            Session["sviKorisnici"] = korisnik1;


            //slanje na front sa back-a
            ViewBag.korisnici = korisnik1;


            return View("Index");
        }
        #endregion

        #region Manifestacija
        [HttpPost]
        public ActionResult FiltriranjeManifestacije(TipManifestacije tipManifestacije)
        {

            //ako necega nema, izbaci gresku
            //fali filter za ono drugo
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Manifestacija> lista = new List<Manifestacija>();


            foreach (var manifestacija in manifestacijas)
            {
                lista.Add(manifestacija);

            }



            List<Manifestacija> manifestacija1 = new List<Manifestacija>();


            foreach (var manifestacija in lista)
            {
                if (manifestacija.TipManifestacije == tipManifestacije)
                {

                    manifestacija1.Add(manifestacija);
                }
            }

            Session["manifestacije"] = manifestacija1;


            //slanje na front sa back-a
            ViewBag.manifestacije = manifestacija1;


            return View("Manifestacija");


        }

        [HttpPost]
        public ActionResult FiltriranjeNerasprodatihManifestacija()
        {
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Manifestacija> lista = new List<Manifestacija>();


            foreach (var manifestacija in manifestacijas)
            {
                lista.Add(manifestacija);

            }



            List<Manifestacija> manifestacija1 = new List<Manifestacija>();


            foreach (var manifestacija in lista)
            {
                if (manifestacija.BrojSlobodnihMesta > 0)
                {

                    manifestacija1.Add(manifestacija);
                }
            }

            Session["manifestacije"] = manifestacija1;


            //slanje na front sa back-a
            ViewBag.manifestacije = manifestacija1;


            return View("Manifestacija");

        }

        [HttpGet]
        public ActionResult SortiranjeNazivManifestacijeOpadajuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Naziv.CompareTo(lista[j].Naziv) < 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Manifestacija");
        }

        [HttpGet]
        public ActionResult SortiranjeNazivManifestacijeRastuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Naziv.CompareTo(lista[j].Naziv) > 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Manifestacija");
        }

        [HttpGet]
        public ActionResult SortiranjeCenaKarteOpadajuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);

            List<Manifestacija> lista2 = new List<Manifestacija>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].CenaKarte.CompareTo(lista[j].CenaKarte) < 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Manifestacija");
        }

        [HttpGet]
        public ActionResult SortiranjeCenaKarteRastuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);

            List<Manifestacija> lista2 = new List<Manifestacija>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].CenaKarte.CompareTo(lista[j].CenaKarte) > 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Manifestacija");
        }
        [HttpGet]
        public ActionResult SortiranjeDatumVremeOdrzavanjaOpadajuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);

            List<Manifestacija> lista2 = new List<Manifestacija>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].DatumVremeOdrzavanja.CompareTo(lista[j].DatumVremeOdrzavanja) < 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Manifestacija");
        }

        [HttpGet]
        public ActionResult SortiranjeDatumVremeOdrzavanjaRastuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);

            List<Manifestacija> lista2 = new List<Manifestacija>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].DatumVremeOdrzavanja.CompareTo(lista[j].DatumVremeOdrzavanja) > 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Manifestacija");
        }


        [HttpGet]
        public ActionResult SortiranjeMestoOdrzavanjaOpadajuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].MestoOdrzavanja.MestoOdrzavanja.Mesto.CompareTo(lista[j].MestoOdrzavanja.MestoOdrzavanja.Mesto) < 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Manifestacija");
        }

        [HttpGet]
        public ActionResult SortiranjeMestoOdrzavanjaRastuce()
        {
            List<Manifestacija> lista = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].MestoOdrzavanja.MestoOdrzavanja.Mesto.CompareTo(lista[j].MestoOdrzavanja.MestoOdrzavanja.Mesto) > 0)
                    {
                        Manifestacija temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = lista;

            return View("Manifestacija");
        }
        #endregion

        #region Karta
        [HttpPost]
        public ActionResult PretragaKarata(string Naziv)
        {

            DateTime DatumVremeOdrzavanjaPocetak = new DateTime();

            DateTime DatumVremeOdrzavanjaKraj = new DateTime();
            int CenaKarteMin;
            int CenaKarteMax;


            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                //odjavljen
                Session["ulogovan"] = null;
                return View("Error");
            }


            //iscitavanje svih korisnik iz kes memorije
            List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);


            List<Karta> karta1 = null;
            List<Karta> karta2 = new List<Karta>();

            if (Naziv != "")
            {
                karta1 = new List<Karta>();
                foreach (var karta in kartas)
                {
                    if (karta.Manifestacija.Naziv.ToLower() == Naziv.ToLower())
                    {
                        karta1.Add(karta);
                    }
                }
            }


            if (Request["DatumVremeOdrzavanjaPocetak"] != "")
            {
                DatumVremeOdrzavanjaPocetak = DateTime.Parse(Request["DatumVremeOdrzavanjaPocetak"]);
                if (karta1 == null)
                {
                    karta1 = new List<Karta>();
                    foreach (var karta in kartas)
                    {

                        if (karta.DatumOdrzavanja >= DatumVremeOdrzavanjaPocetak)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
                else
                {

                    karta2 = karta1;
                    karta1 = new List<Karta>();

                    foreach (var karta in karta2)
                    {

                        if (karta.DatumOdrzavanja >= DatumVremeOdrzavanjaPocetak)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
            }

            if (Request["DatumVremeOdrzavanjaKraj"] != "")
            {
                DatumVremeOdrzavanjaKraj = DateTime.Parse(Request["DatumVremeOdrzavanjaKraj"]);
                if (karta1 == null)
                {
                    karta1 = new List<Karta>();
                    foreach (var karta in kartas)
                    {

                        if (karta.DatumOdrzavanja <= DatumVremeOdrzavanjaKraj)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
                else
                {

                    karta2 = karta1;
                    karta1 = new List<Karta>();

                    foreach (var karta in karta2)
                    {

                        if (karta.DatumOdrzavanja <= DatumVremeOdrzavanjaKraj)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
            }

            if (Request["CenaKarteMin"] != "" && Request["CenaKarteMin"] != null)
            {
                CenaKarteMin = Int32.Parse(Request["CenaKarteMin"]);
                if (karta1 == null)
                {
                    karta1 = new List<Karta>();
                    foreach (var karta in kartas)
                    {

                        if (karta.Cena >= CenaKarteMin)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
                else
                {

                    karta2 = karta1;
                    karta1 = new List<Karta>();

                    foreach (var karta in kartas)
                    {

                        if (karta.Cena >= CenaKarteMin)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
            }

            if (Request["CenaKarteMax"] != "" && Request["CenaKarteMax"] != null)
            {
                CenaKarteMax = Int32.Parse(Request["CenaKarteMax"]);
                if (karta1 == null)
                {
                    karta1 = new List<Karta>();
                    foreach (var karta in kartas)
                    {

                        if (karta.Cena <= CenaKarteMax)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
                else
                {

                    karta2 = karta1;
                    karta1 = new List<Karta>();

                    foreach (var karta in karta2)
                    {

                        if (karta.Cena <= CenaKarteMax)
                        {
                            karta1.Add(karta);
                        }
                    }
                }
            }
            if (karta1 == null)
            {
                karta1 = kartas;
            }
            ViewBag.karte = karta1;

            return View("Karte");


        }
        [HttpPost]
        public ActionResult FiltriranjeTipKarte(Tip TipKarte)
        {
            
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }

            
            List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();


            foreach (var karta in kartas)
            {
                lista.Add(karta);

            }

            List<Karta> karta1 = new List<Karta>();


            foreach (var karta in lista)
            {
                if (karta.Tip == TipKarte)
                {
                    karta1.Add(karta);
                }
            }


            Session["karte"] = karta1;


            //slanje na front sa back-a
            ViewBag.karte = karta1;


            return View("Karte");
        }

        [HttpPost]
        public ActionResult FiltriranjeStatusKarte(string StatusKarte)
        {
            if (((Korisnik)Session["ulogovan"]).Uloga != Uloga.Administrator)
            {
                Session["ulogovan"] = null;
                return View("Error");
            }


            List<Karta> kartas = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista = new List<Karta>();

            bool rezervisana;

            if (StatusKarte == "Rezervisana")
                rezervisana = false;
            else
                rezervisana = true;

            foreach (var karta in kartas)
            {
                lista.Add(karta);

            }

            List<Karta> karta1 = new List<Karta>();


            foreach (var karta in lista)
            {
                if (karta.Status == rezervisana)
                {
                    karta1.Add(karta);
                }
            }


            Session["karte"] = karta1;


            //slanje na front sa back-a
            ViewBag.karte = karta1;


            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeNazivOpadajuce()
        {
            List<Karta> lista = (List<Karta>)(HttpContext.Cache["karte"]);
            

            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    
                    if (lista[i].Manifestacija.Naziv.CompareTo(lista[j].Manifestacija.Naziv) < 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.karte = lista;
            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeNazivRastuce()
        {
            List<Karta> lista = (List<Karta>)(HttpContext.Cache["karte"]);

            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Manifestacija.Naziv.CompareTo(lista[j].Manifestacija.Naziv) > 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.karte = lista;
            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeVremeOpadajuce()
        {
            List<Karta> lista = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista2 = new List<Karta>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].DatumOdrzavanja.CompareTo(lista[j].DatumOdrzavanja) < 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.karte = lista;

            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeVremeRastuce()
        {
            List<Karta> lista = (List<Karta>)(HttpContext.Cache["karte"]);
            List<Karta> lista2 = new List<Karta>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].DatumOdrzavanja.CompareTo(lista[j].DatumOdrzavanja) > 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.karte = lista;

            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeCenaOpadajuce()
        {
            List<Karta> lista = (List<Karta>)(HttpContext.Cache["karte"]);

            List<Karta> lista2 = new List<Karta>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Cena.CompareTo(lista[j].Cena) < 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.karte = lista;

            return View("Karte");
        }

        [HttpGet]
        public ActionResult SortiranjeCenaRastuce()
        {
            List<Karta> lista = (List<Karta>)(HttpContext.Cache["karte"]);

            List<Karta> lista2 = new List<Karta>();


            for (int i = 0; i < lista.Count - 1; i++)
            {
                for (int j = i + 1; j < lista.Count; j++)
                {
                    if (lista[i].Cena.CompareTo(lista[j].Cena) > 0)
                    {
                        Karta temp = lista[i];
                        lista[i] = lista[j];
                        lista[j] = temp;

                    }
                }
            }

            ViewBag.karte = lista;

            return View("Karte");
        }
        #endregion

        #region Komentar

        [HttpPost]
        public ActionResult PrikazKomentara(int Id)
        {
            List<Komentar> komentars = (List<Komentar>)(HttpContext.Cache["komentari"]);
            List<Komentar> sviKomentari = new List<Komentar>();

            foreach(var item in komentars)
            {
                if (item.Manifestacija == Id)
                    sviKomentari.Add(item);
            }

            ViewBag.komentari = sviKomentari;
            return View("Komentari");
        }
        #endregion
    }
}