﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web1_Projekat.Controllers
{
    public class LogInController : Controller
    {
        Baza baza = new Baza();

        // GET: LogIn
        public ActionResult Index()
        {

            return View();
        }

        [HttpGet]
        public ActionResult PocetnaStrana()
        {
            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            List<Manifestacija> aktivnaListaManifestacija = new List<Manifestacija>();


            foreach (var manifestacija in manifestacijas)
            {
                if (manifestacija.Status == true)
                {
                    aktivnaListaManifestacija.Add(manifestacija);
                }
            }


            for (int i = 0; i < aktivnaListaManifestacija.Count - 1; i++)
            {
                for (int j = i + 1; j < aktivnaListaManifestacija.Count; j++)
                {
                    if (aktivnaListaManifestacija[i].DatumVremeOdrzavanja.CompareTo(aktivnaListaManifestacija[j].DatumVremeOdrzavanja) < 0)
                    {
                        Manifestacija temp = aktivnaListaManifestacija[i];
                        aktivnaListaManifestacija[i] = aktivnaListaManifestacija[j];
                        aktivnaListaManifestacija[j] = temp;

                    }
                }
            }

            ViewBag.manifestacije = aktivnaListaManifestacija;
            Session["manifestacije"] = aktivnaListaManifestacija;

            return View();
        }

        [HttpGet]
        public ActionResult Manifestacija()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PrikazManifestacije(int id)
        {

            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            
            foreach (var item in manifestacijas)
            {
                if (id == item.Id)
                {
                    ViewBag.Manifestacija = item;
                }
            }

            

            return View();
        }


        [HttpGet]
        public ActionResult SveManifestacije()
        {

            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);

            for (int i = 0; i < manifestacijas.Count - 1; i++)
            {
                for (int j = i + 1; j < manifestacijas.Count; j++)
                {
                    if (manifestacijas[i].DatumVremeOdrzavanja.CompareTo(manifestacijas[j].DatumVremeOdrzavanja) < 0)
                    {
                        Manifestacija temp = manifestacijas[i];
                        manifestacijas[i] = manifestacijas[j];
                        manifestacijas[j] = temp;

                    }
                }
            }



            ViewBag.manifestacije = manifestacijas;
            Session["manifestacije"] = manifestacijas;


            return View();
        }

        [HttpPost]
        public ActionResult LogIn()
        {

            string korisnickoime = Request["KorisnickoIme"];
            string pass = Request["Lozinka"];

            Dictionary<string, Korisnik> listaSvihKorisnika = (Dictionary<string, Korisnik>)(HttpContext.Cache["korisnici"]);
            
            if (listaSvihKorisnika.ContainsKey(korisnickoime))
            {
                if (listaSvihKorisnika[korisnickoime].Lozinka == pass)
                {
                    Session["ulogovan"] = listaSvihKorisnika[korisnickoime];

                    
                    if (listaSvihKorisnika[korisnickoime].Uloga == Uloga.Administrator)
                    {
                       
                            return RedirectToAction("Index", "Administrator");

                    }
                    else if (listaSvihKorisnika[korisnickoime].Uloga == Uloga.Kupac)
                    {
                        
                            return RedirectToAction("Index", "Kupac");

                    }
                    else
                    {
                        
                            return RedirectToAction("Index", "Prodavac");

                    }
                

                }
            }
            return View("Index");


        }

        [HttpPost]
        public ActionResult RegistrujSe(Korisnik korisnik)
        {

            Dictionary<string, Korisnik> listaSvihKorisnika = (Dictionary<string, Korisnik>)(HttpContext.Cache["korisnici"]);

            if (Session["ulogovan"] != null)
            {
                if (listaSvihKorisnika.ContainsKey(korisnik.KorisnickoIme))
                {
                    return View("Registration");
                }

                korisnik.Uloga = Uloga.Prodavac;
                if (korisnik.DatumRodjenja > DateTime.Now)
                {
                    return View("Registration");
                }
                listaSvihKorisnika.Add(korisnik.KorisnickoIme, korisnik);
                baza.ListaKorisnika(listaSvihKorisnika);

                HttpContext.Cache["korisnici"] = listaSvihKorisnika;

                return RedirectToAction("Index", "Administrator");
            }

            if (listaSvihKorisnika.ContainsKey(korisnik.KorisnickoIme))
            {
                return View("Registration");
            }

            korisnik.Uloga = Uloga.Kupac;
            if(korisnik.DatumRodjenja > DateTime.Now)
            {
                return View("Registration");
            }
            listaSvihKorisnika.Add(korisnik.KorisnickoIme, korisnik);
            baza.ListaKorisnika(listaSvihKorisnika);

            HttpContext.Cache["korisnici"] = listaSvihKorisnika;
            

            return View("Index");
        }

        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Profil()
        {
            if (Session["ulogovan"] == null)
            {
                return View("Error");
            }
            ViewBag.Korisnik = (Korisnik)Session["ulogovan"];

            return View();
        }

        [HttpGet]
        public ActionResult Odjava()
        {
            Session["ulogovan"] = null;
            return View("Index");
        }

        [HttpGet]
        public ActionResult IzmenaProfila()
        {
            if (Session["ulogovan"] == null)
            {
                return View("Error");
            }
            ViewBag.Korisnik = (Korisnik)Session["ulogovan"];


            return View();
        }

        [HttpPost]
        public ActionResult SacuvajIzmeneProfila(Korisnik korisnik)
        {
            Korisnik kor = (Korisnik)Session["ulogovan"];

            Dictionary<string, Korisnik> listaSvihKorisnika = (Dictionary<string, Korisnik>)(HttpContext.Cache["korisnici"]);


            if (kor.KorisnickoIme != korisnik.KorisnickoIme && listaSvihKorisnika.ContainsKey(korisnik.KorisnickoIme))
            {
                ViewBag.Korisnik = korisnik;
                ViewBag.Greska = "Korisnicko ime vec zauzeto";
                return View("IzmenaProfila");
            }




            korisnik.Pol = kor.Pol;
            korisnik.BrojSakupljenihBodova = kor.BrojSakupljenihBodova;
            korisnik.Manifestacija = kor.Manifestacija;
            korisnik.SveKarte = kor.SveKarte;
            korisnik.TipKorisnika = kor.TipKorisnika;
            korisnik.Uloga = kor.Uloga;

            listaSvihKorisnika.Remove(kor.KorisnickoIme);

            listaSvihKorisnika.Add(korisnik.KorisnickoIme, korisnik);
            HttpContext.Cache["korisnici"] = listaSvihKorisnika;
            baza.ListaKorisnika(listaSvihKorisnika);
            Session["ulogovan"] = korisnik;

            return RedirectToAction("Profil");
        }

        [HttpPost]
        public ActionResult DetaljiManifestacije(int Id)
        {

            List<Manifestacija> manifestacijas = (List<Manifestacija>)(HttpContext.Cache["manifestacije"]);
            Manifestacija manifestacija = new Manifestacija();

            List<Komentar> komentars = (List<Komentar>)(HttpContext.Cache["komentari"]);
            List<Komentar> sviKomentari = new List<Komentar>();

            foreach (var manifestacija4 in manifestacijas)
            {
                if (manifestacija4.Id == Id)
                {
                    manifestacija = manifestacija4;
                }
            }
            foreach (var item in komentars)
            {
                if (item.Manifestacija == Id && item.Status == true)
                {
                    sviKomentari.Add(item);
                }
            }

            ViewBag.komentari = sviKomentari;


            ViewBag.Manifestacija = manifestacija;

            return View();
        }

    }
}